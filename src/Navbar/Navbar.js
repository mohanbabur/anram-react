import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import AuthService from '../Services/Auth/Auth-service';
import Tokenserv from '../Token/Token-service'
class Navbar extends Component {
    constructor() {
        super();

        this.state = {
            isLoggedIn: false
        }

        AuthService.subject.subscribe(isLoggedIn => {
            this.setState({ isLoggedIn })
        })

    }

    handleRemoveToken() {
        Tokenserv.removeToken();
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light navbar-wrapper navbar-inner">
                <img className="navbar-logo-style " src="/Assets/Images/anram.png" alt="pic is not available" />
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <i className="fa fa-bars text-white"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav ml-lg-auto text-center">
                        <li className="nav-item active">
                            <Link className="nav-link nav-bar navitem" to="/">HOME
                               <span className="sr-only">(current)</span>
                            </Link>
                        </li>
                        {
                            !this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="about">ABOUT US</Link>
                            </li>
                        }
                        {
                            !this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="skills">OUR SKILLS</Link>
                            </li>
                        }
                        {
                            !this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="project">SERVICES</Link>
                            </li>
                        }

                        {
                            !this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="team">TEAM</Link>
                            </li>
                        }

                        {
                            !this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="contact">CONTACT US</Link>
                            </li>
                        }
                        {
                            !this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="user">CREATE USER</Link>
                            </li>
                        }
                        {
                            !this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="login">SIGN IN</Link>
                            </li>
                        }

                        {
                            this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="attendance">Attendanceportal</Link>
                            </li>
                        }

                        {
                            this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="userprofile">USERPROFILE</Link>
                            </li>
                        }

                        {
                            this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="leave">LEAVEMANGEMENT</Link>
                            </li>
                        }

                        {
                            this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="leaves">LEAVELIST</Link>
                            </li>
                        }

                        {
                            this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="userlist">USERSLIST</Link>
                            </li>
                        }

                        {
                            this.state.isLoggedIn && <li className="nav-item active">
                                <Link className="nav-link nav-bar navitem" to="leavelistprofile">LEAVEPROFILE</Link>
                            </li>
                        }

                        {
                            this.state.isLoggedIn && <li className="nav-item active">
                                <Link onClick={this.handleRemoveToken} className="nav-link nav-bar navitem" to="login"><i className=" signout-Icon fa fa-power-off"></i>
                                </Link>
                            </li>
                        }
                    </ul>
                </div>
            </nav>
        )
    }

}


export default Navbar;