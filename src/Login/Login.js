import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Userserv from '../Services/Auth/Auth-service';
import { withRouter } from 'react-router'
import Tokenserv from '../Token/Token-service';
import './Login.css';
import Swal from 'sweetalert2';

class Login extends Component {
    data = {};
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(event) {
        this.data[event.target.name] = event.target.value;

    }
  
    async handleSubmit(event) {
        event.preventDefault();
        try {
            const result = await Userserv.userLogin(this.data);
            Swal.fire({
                position: 'top-center',
                type: 'success',
                title: 'You have logged in successfully',
                showConfirmButton: false,
                timer: 1500
            })
            Userserv.subject.next(true);
            Tokenserv.setToken(result.data.token);
            this.props.history.push('leave');
          
        }
        catch (error) {
            Swal.fire({
                position: 'top-center',
                type: 'error',
                title: 'Please Enter valid Email and Passwords',
                showConfirmButton: false,
                timer: 1500
            })
        }

   
    }
    render() {
        return (
            <div className="container-fluid main-content login-bg-color">
                <div className="row justify-content-center">
                    <div className="col-md-4 login-formcolor mt-5 ">
                        <div className="formborder">
                            <h3 className="text-center text-white mt-5">CONNECT US</h3>
                            <div className="row justify-content-center">
                                <div className="col-md-4">
                                    <hr style={{ 'border': 'none' }} />
                                </div>

                            </div>
                            <form className="mt-3" name="loginform" style={{ 'maxWidth': '500px', 'margin': 'auto' }} onSubmit={this.handleSubmit}>
                                <div className="login-input-container">
                                    <input className="login-input-field" type="email" placeholder="Email" name="Email" required onChange={this.handleChange} />
                                </div>

                                <div className="login-input-container">
                                    <input className="login-input-field" type="password" placeholder="Password" name="Password" required onChange={this.handleChange} />

                                </div>

                                <button type="Submit" className="login-btn login-input-field">LOGIN IN</button>
                            </form>
                            <div className="row mt-4 justify-content-center ">
                                <div className="col-md-6">
                                    <Link className="text-white text-center" to="/acc/change">CHANGE PASSWORD</Link>
                                    {/* <hr style={{ 'border': 'none', 'border-bottom': '3px solid red' }} /> */}
                                </div>
                                <div className="col-md-6">
                                    <Link className="text-white" to="/acc/forgot">FORGOT PASSWORD?</Link>
                                    {/* <hr style={{ 'border': 'none', 'border-bottom': '3px solid red' }} /> */}
                                </div>

                                {/* <div className="row mt-4 ">
                                    <p style={{ 'color': 'red', 'opacity': '1', 'font-size': '20px' }} >Invalid username or password</p>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Login);