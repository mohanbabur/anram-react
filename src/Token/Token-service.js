
class Tokenserv {

    setToken(token) {
        console.log(token);
        localStorage.setItem('Token', token);

    }

    get token() {
        return localStorage.getItem('Token');
    }

    removeToken() {
        localStorage.removeItem('Token');
    }

    tokenDecoded() {

        const Token = localStorage.getItem("Token");

        if (!Token) {
            return { userRole: 'user' };
        }
        else {
            const splitdata = Token.split('.')[1];
            const decoded = JSON.parse(window.atob(splitdata));
            console.log(decoded)
            return decoded;
        }

    }
}

export default new Tokenserv();