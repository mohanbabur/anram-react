import React, { Component } from 'react';
import Userleave from '../Services/Leavemanagement/Leave-service';
import './Leavelist.css';
class Leavelist extends Component {

    state = {
        datas: []
    }
    constructor() {
        super();
        this.getUserList = this.getUserList.bind(this);
        this.getUserList();

    }

    async getUserList() {
        const result = await Userleave.leaveList();
        this.setState({ datas: result.data });
    }


    render() {
        return (
            <div className="container-fluid">
                <div className="row justify-content-center mt-3">
                    <div className="col-md-12 mt-5">
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th className="text-center" scope="col">EmployeeId</th>
                                    <th className="text-center" scope="col">FromDate</th>
                                    <th className="text-center" scope="col">Todate</th>
                                    <th className="text-center" scope="col">Description</th>
                                    <th className="text-center" scope="col">Status</th>
                                    <th className="text-center" scope="col">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.datas.map(data => (
                                        <tr>
                                            <td className="text-center">{data.employeeId}</td>
                                            <td className="text-center">{(new Date(data.from)).toDateString()}</td>
                                            <td className="text-center">{(new Date(data.to)).toDateString()}</td>
                                            <td className="text-center">{data.description}</td>
                                            <td className="text-center">{data.status}</td>
                                            <td>
                                                <button className="leave-list-action-btn text-success ml-8">
                                                    <i className="fa fa-edit" aria-hidden="true"></i>
                                                </button>
                                                <button className="leave-list-action-btn text-danger">
                                                    <i className="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div >

        )
    }
}

export default Leavelist;