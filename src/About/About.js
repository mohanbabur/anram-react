import React, { Component } from 'react';
import './About.css';
class About extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center mt-5 content-height ">
                    <div className="col-12">
                        <div className="about-page">
                            <h4 className="text-center"><label className="text-danger">ABOUT</label> US</h4>
                            <p>ANRAM SOLUTIONS is a new age company that provides quality IT services and Integrated solutions to the Industry across the globe, We offer a wide range of business solutions in general and services in the areas of Customized Software Development, Consulting and Workforce Solutions, in particular.</p>
                            <p>At Anram, we understand that our client's needs are ever changing and needs constant attentions in terms of process and people which are a company's most important elements. Our goal is to refine the business process through our expertise and knowledge, align people, thus ensuring that our clienteles are engaged with customized process and people that will propel their organization towards success.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default About;