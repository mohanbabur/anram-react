import axios from 'axios';
import { BehaviorSubject } from 'rxjs';
import Tokenserv from '../../Token/Token-service'

class Authserv {
subject = new BehaviorSubject(false);
    createUser(data) {
        const url = 'http://localhost:2021/users';
        return axios.post(url, data);
    }

    userLogin(data) {
        const url = 'http://localhost:2021/getin';
        return axios.post(url, data);
    }

    getUserId() {
        const data = Tokenserv.tokenDecoded();
        const id = data.id;
        const url = `http://localhost:2021/getid/${id}`;
        return axios.get(url)
    }

    getUserList() {
        const url = 'http://localhost:2021/users'
        return axios.get(url)
    }

    userAttendance() {
        const data = Tokenserv.tokenDecoded();
        const EmployeeId ={employeeId: data.employeeId} ;
        console.log(EmployeeId);
        const url = 'http://localhost:2021/punch';
        return axios.post(url, EmployeeId);
        
    }
    userAttendanceEnd() {
        const data = Tokenserv.tokenDecoded();
        const employeeId = data.employeeId;
        // console.log(EmployeeId);
        const url = `http://localhost:2021/punch/${employeeId}`;
        return axios.put(url);
    }
}


export default new Authserv();