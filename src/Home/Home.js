import React, { Component, Fragment } from 'react';
import './Home.css';
import Ourservice from '../Ourservices/Services';
import Ourskills from '../Ourskills/Ourskills';
import Ourteam from '../Ourteam/Ourteam';
import Contact from '../Contactus/Contactus';
class Home extends Component {
    render() {
        return (
            <Fragment>
                <div className="img-style">
                    <div className="carousel-image">
                        <div id="carouselExampleIndicators" className="carousel slide carousel-img " data-ride="carousel">
                            <ol className="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img className="d-block w-100 img-sty " src="/Assets/Images/bg.jpg"
                                        alt="First slide" />
                                </div>
                                <div className="carousel-item">
                                    <img className="d-block w-100 img-sty " src="/Assets/Images/bg1.jpg"
                                        alt="Second slide" />
                                </div>
                                <div className="carousel-item">
                                    <img className="d-block w-100 img-sty " src="/Assets/Images/bg2.jpg"
                                        alt="Third slide" />
                                </div>
                            </div>
                            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>

                <Ourservice />
                <Ourskills />
                <Ourteam />
                <Contact />
            </Fragment>

        )
    }
}


export default Home;