import React, { Component } from 'react';
import Userserv from '../Services/Auth/Auth-service';
import './Userprofile.css';

class Userprofile extends Component {
    state = {
        FatherName: '',
        EmployeeName: '',
        Address: '',
        AdhaarNo: '',
        Email: '',
        EmergencyContactNo: '',
        EmployeeContactNo: '',
        Gender: '',
        PancardNo: '',
        PermananentAddress: '',
        employeeId: ''
    };

    datas = {}
    async componentDidMount() {
        const data = await Userserv.getUserId();
        this.datas = data;
        this.setState({ EmployeeName: this.datas.data.EmployeeName,
            FatherName: this.datas.data.FatherName,
            AdhaarNo: this.datas.data.AdhaarNo,
            PancardNo: this.datas.data.PancardNo,
            Address: this.datas.data.Address,
            EmployeeContactNo: this.datas.data.EmployeeContactNo,
            EmergencyContactNo: this.datas.data.EmergencyContactNo
          })
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="row justify-content-center mt-5">
                    <div className="col-md-4 text-center">
                        <img src="/Assets/Images/Avatar.png" alt="avatar" className="img-responsive w-50 user-avathar" />
                        <div className="mt-3 text-left">
                            <h4 className="text-center"><b>Profile</b></h4>
                            <div className="pl-5 mt-3 user-profile-list">
                                <p>
                                    <span>EmployeeName :</span>
                                    <b>{this.state.EmployeeName}</b>
                                </p>
                                <p>
                                    <span>Father Name :</span>
                                    <b>{this.state.FatherName}</b>
                                </p>
                                <p>
                                    <span>Adhaar No :</span>
                                    <b>{this.state.AdhaarNo} </b>
                                </p>
                                <p>
                                    <span>Pancard :</span>
                                    <b> {this.state.PancardNo}</b>
                                </p>
                                <p>
                                    <span>Address :</span>
                                    <b>{this.state.Address} </b>
                                </p>
                                <p>
                                    <span>Contact No :</span>
                                    <b> {this.state.EmployeeContactNo}</b>
                                </p>
                                <p>
                                    <span>Emergency No :</span>
                                    <b> {this.state.EmergencyContactNo}</b>
                                </p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Userprofile;