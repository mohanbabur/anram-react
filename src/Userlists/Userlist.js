import React, { Component } from 'react';
import Userserv from '../Services/Auth/Auth-service';
class Userlists extends Component {
    state = {
        userdatalist: []
    }
    constructor() {
        super();
        
        this.getUserDataList = this.getUserDataList.bind(this);
         this.getUserDataList();
       
    }
    async getUserDataList() {
        const userdata = await Userserv.getUserList();
        this.setState({userdatalist: userdata.data })
    }
    render() {

        return (
            <div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th className="text-center">EmployeeId</th>
                            <th className="text-center">EmployeeName</th>
                            <th className="text-center">FatherName</th>
                            <th className="text-center">Email</th>
                            <th className="text-center">Address</th>
                            <th className="text-center">EmployeeContactNo</th>
                            <th className="text-center">EmergencyContactNo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                                    this.state.userdatalist.map(data => (
                                        <tr>
                                            <td className="text-center">{data.employeeId}</td>
                                            <td className="text-center">{data.EmployeeName}</td>
                                            <td className="text-center">{data.FatherName}</td>
                                            <td className="text-center">{data.Email}</td>
                                            <td className="text-center">{data.Address}</td>
                                            <td className="text-center">{data.EmployeeContactNo}</td>
                                            <td className="text-center">{data.EmergencyContactNo}</td>
                                        </tr>
                                    ))
                                }
                    </tbody>
                </table>
            </div>

        )
    }
}


export default Userlists;