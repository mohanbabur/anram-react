import React, { Component } from 'react';
import './Leave.css';
import Userleave from '../Services/Leavemanagement/Leave-service';
import Swal from 'sweetalert2';

class Leavemanagement extends Component {
    data = {};
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.data[event.target.name] = event.target.value
    }

    async  handleSubmit(event) {
        event.preventDefault();
        event.target.reset();
        try {
             await Userleave.userLeave(this.data);
            Swal.fire({
                position: 'top-center',
                type: 'success',
                title: 'Leave Request is submitted successfully',
                showConfirmButton: false,
                timer: 3000
            })
        }

        catch (error) {

        }

    }
    render() {
        return (
            <div className="container-fluid">
                <div className="row justify-content-center leave-bg-color">
                    <div className="col-md-4 mt-3 leave-formcolor">
                        <div className="formborder">
                            <h3 className="text-center text-white mt-2">LEAVE FORM</h3>
                            <hr />
                            <form name="leaveform" onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label className="leave-label-style">EmployeeId:</label>
                                    <input className="leave-input-field" type="text"
                                        name="employeeId" required onChange={this.handleChange} />
                                </div>

                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="leave-label-style">From:</label>
                                            <input className="leave-input-field " type="date" name="from"
                                                required onChange={this.handleChange} />
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label-style">To:</label>
                                            <input className="leave-input-field " type="date" name="to"
                                                required onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="leave-label-style">LeaveType</label>
                                    <select className="leave-input-field" name="leaveType" id="LeaveType" onChange={this.handleChange} required>
                                        <option>Sick Leave</option>
                                        <option>Personal Leave</option>
                                        <option>Planning Leave</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label className="leave-label-style">Description</label>
                                    <textarea className="leave-input-field" name="description"
                                        id="exampleFormControlTextarea1" rows="3" onChange={this.handleChange} required></textarea>
                                </div>
                                <button type="submit" className="leave-btn leave-input-field ">SUBMIT</button>
                                <div className="row mt-4">
                                </div>
                            </form>
                            <div>
                            </div>
                        </div>
                    </div >
                </div>
            </div>
        )
    }
}

export default Leavemanagement;