import React, { Component } from 'react';
import './Heading.css';
class Heading extends Component {

    render() {
        return (
            <h3 className="text-center main-heading">
                {this.props.children}
            </h3>

        )
    }

}

export default Heading;