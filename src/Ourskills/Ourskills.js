import React, { Component } from 'react';
import './Ourskills.css';
import Heading from '../Mainheading/Heading';

class Ourskills extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row mt-5" id="skills">
                    <div className="col-md-12 ourskills-bck-img">
                        <div>
                            <div>
                                <Heading>
                                    <span className="m-3 text-white">OUR</span>
                                    <span>SKILLS</span>
                                </Heading>
                                <div className="row justify-content-center">
                                    <div className="col-md-4 mt-3">
                                        <h2 className="text-white text-center">Proffessional Skills</h2>
                                        <p className="text-white">Excepteur sint occaecat non proident, sunt in culpa quis. Int Phasellus lacinia id
                                          erat eu ullamcorper. Nunc id ipsum eur fringillats, gravida felis vitae. Phasellus lacinia id, sunt in
                                          culp quis. Phasellus lacinia. gravida felis vitae. Phasellus lacinia id. sunt inculpa quis. Phasellus
                                          lacinia. gravida felis vitae.</p>
                                    </div>
                                    <div className="col-md-4 mt-3">
                                        <label className="text-white">PHP DEVELOPERS:</label>
                                        <div className="progress">
                                            <div className="ourskills-progress-bar progress-bar-striped" style={{ 'width': '30%' }}></div>
                                        </div>
                                        <br />
                                        <label className="text-white">ANDROID DEVELOPERS:</label>
                                        <div className="progress">
                                            <div className="ourskills-progress-bar bg-success progress-bar-striped" style={{ 'width': '40%' }}></div>
                                        </div>
                                        <br />
                                        <label className="text-white">ANGULAR DEVELOPERS:</label>
                                        <div className="progress">
                                            <div className="ourskills-progress-bar bg-info progress-bar-striped" style={{ 'width': '50%' }}></div>
                                        </div>
                                        <br />
                                        <label className="text-white">NODE DEVELOPERS:</label>
                                        <div className="progress">
                                            <div className="ourskils-progress-bar bg-warning progress-bar-striped" style={{ 'width': '60%' }}></div>
                                        </div>
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Ourskills;