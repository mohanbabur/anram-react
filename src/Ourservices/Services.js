import React, { Component } from 'react';
import './Services.css';
import Heading from '../Mainheading/Heading';
class Ourservices extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row mt-3 justify-content-center" id="project">
                    <div className="col-12">
                        <Heading>
                            <span className="m-3">OUR</span>
                            <span>SERVICES</span>
                        </Heading>
                    </div>
                </div>

                <div className="row justify-content-center">
                    <div className="col-md-3">
                        <div>
                            <img className="ourservice-img-center" src="/Assets/Images/Digital-Woekplace.gif" alt="pic is not available" />
                            <h6 className="text-center font-weight-bold text-danger">CUSTOM SOFTWARE DEVELOPMENT</h6>
                            <p className="text-center">Custom software development services allow businesses to transform their daily
                              operations
                                            into specific goal et...</p>
                            <hr style={{ 'border': 'none', 'borderBottom': '3px solid red' }} />
                        </div>
                    </div>

                    <div className="col-md-3">
                        <div>
                            <img className="ourservice-img-center" src="/Assets/Images/Digital-Application-1.gif" alt="pic is not available" />
                            <h6 className="text-center font-weight-bold text-danger">MOBILITY</h6>
                            <p className="text-center">Smart phones are omnipotent. Moreover, as we are getting adapted to the wide-spread
                                     concept et...</p>
                            <hr style={{ 'border': 'none', 'borderBottom': '3px solid red' }} />
                        </div>
                    </div>

                    <div className="col-md-3">
                        <div>
                            <img className="ourservice-img-center" src="/Assets/Images/Digital-infrastructure-2.gif" alt="pic is not available" />
                            <h6 className="text-center text-danger text-bold font-weight-bold">TESTING</h6>
                            <p className="text-center">Software testing involves the execution of a software component or system component to
                                        evaluate one et...</p>
                            <hr style={{ 'border': 'none', 'borderBottom': '3px solid red' }} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default Ourservices;