import React, { Component } from 'react';
import './Contactus.css';
import { Link } from "react-router-dom";
import Heading from '../Mainheading/Heading';

class Contact extends Component {
  render() {
    return (
      <div className="container-fluid" >
        <section className="row mt-5" id="contact">
          <section className="col-md-12 contact-bg-img">
            <div className="row contact-background-color">
              <section className="col-12">
                <Heading>
                  <span className="m-3 text-white">CONTACT</span>
                  <span>US</span>
                </Heading>
                <h6 className="text-center text-white mt-4">DON'T HESITATE TO CONTACT US</h6>

                <form className="row justify-content-center mt-5">

                  <section className="col-md-4">
                    <div className="form-group ">
                      <input type="text" className="form-control  " id="name" placeholder="Enter name" name="name" />
                    </div>
                    <div className="form-group">
                      <input type="email" className="form-control" id="email" placeholder="Enter email" name="email" />
                    </div>
                    <div className="form-group ">
                      <input type="text" className="form-control" id="subject" placeholder="Enter subject" name="subject" />
                    </div>
                  </section>


                  <section className="col-md-4">
                    <div className="form-group">
                      <textarea className="form-control" rows="3" placeholder="enter the comments" id="comment"></textarea>
                    </div>
                    <button type="submit" className="btn mt-1 contact-btn">Submit</button>
                  </section>
                </form>


                <section className="text-center">
                  <span className="contact-text-or mx-auto d-block">OR</span>
                </section>
              </section>


              <section className="col-md-12 mt-3">
                <p className="font-weight-bold text-center text-white">FIND US ON SOCIAL MEDIA</p>
                <ul className="social-icon-list list-unstyled contact-list-inline text-center">
                  <li className="conatct-li contact-list-inline">
                    <Link className="contact-a-style" to="https://www.facebook.com/vertisize">
                      <i className="fa fa-facebook contact-socialicons"></i>
                    </Link>
                  </li>
                  <li className="conatct-li contact-list-inline">
                    <Link className="contact-a-style" to="https://twitter.com/vertisizeTech">
                      <i className="fa fa-twitter contact-socialicons"></i>
                    </Link>
                  </li>
                  <li className="conatct-li contact-list-inline">
                    <Link className="contact-a-style" to="https://www.google.com/search?q=vertisize+solutions">
                      <i className="fa fa-google-plus contact-socialicons"></i>
                    </Link>
                  </li>
                  <li className="conatct-li contact-list-inline">
                    <Link className="contact-a-style" to="https://www.pinterest.com/vertisizesoluti">
                      <i className="fa fa-pinterest contact-socialicons"></i>
                    </Link>
                  </li>
                </ul>
              </section>
            </div>
          </section>
        </section>
      </div>
    )
  }
}

export default Contact;