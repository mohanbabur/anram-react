import React, { Component } from 'react';
import './Attendanceportal.css';
import Swal from 'sweetalert2';


import Userserv from '../Services/Auth/Auth-service';
class Attendance extends Component {

  state = {
    buttonToggle: ""
  }

  constructor() {
    super();

    this.userEndPunch = this.userEndPunch.bind(this);
    this.userStartPunch = this.userStartPunch.bind(this);

    // this.userStartPunch();
    // this.userEndPunch();

  }


  async  userStartPunch() {
    try {
      const data = await Userserv.userAttendance();
      this.setState({ buttonToggle: true });
      Swal.fire({
        position: 'top-center',
        type: 'success',
        title: 'Your Intime Is Marked Successfully',
        showConfirmButton: false,
        timer: 3000
      })
    }

    catch {
      Swal.fire({
        position: 'top-center',
        type: 'error',
        title: 'Sorry you have already punched',
        showConfirmButton: false,
        timer: 3000
      })
    }


  }

  async userEndPunch() {

    try {
      const endtime = await Userserv.userAttendanceEnd();
      this.setState({ buttonToggle: false })
      Swal.fire({
        position: 'top-center',
        type: 'success',
        title: 'Your Outtime Is Marked Successfully',
        showConfirmButton: false,
        timer: 3000
      })
    }

    catch {
      Swal.fire({
        position: 'top-center',
        type: 'error',
        title: 'Sorry you have already punched Endtime',
        showConfirmButton: false,
        timer: 3000
      })
    }

  }
  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-5">
          <div className="col-md-12">
            <h4 className="text-center">Attendance Portal</h4>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-4 mt-5">
            <img src="/Assets/Images/Avatar.png" alt="avatar" className="img-responsive w-50 Attendance-avathar" />

          </div>
        </div>
        <div className="row mt-5 justify-content-center">
          <div className="col-md-4">



            <div className="mt-5">
              {!this.state.buttonToggle && <button className="btn mx-auto d-block" onClick={this.userStartPunch} type="button" >StartTime</button>}
              {this.state.buttonToggle && <button className="btn mx-auto d-block" onClick={this.userEndPunch} type="button">End Time</button>}

            </div>
          </div>

          {/* <div class="col-md-6">

            <div class="vertical-style">

              <h4 class="text-center">ANRAM POLICIES</h4>

            </div>

          </div> */}

        </div>

      </div>
    )
  }
}


export default Attendance;