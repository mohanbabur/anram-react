import axios from 'axios';

class Userleave {
     userLeave(data) {
        console.log(data);
        const url = 'http://localhost:2021/leave';
        return axios.post(url, data);
    }

    leaveList() {
        const url = 'http://localhost:2021/leave';
        return axios.get(url);
    }
}


export default new Userleave();