import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import './App.css';
import Navbar from './Navbar/Navbar';
import About from './About/About';
import Footer from './Footer/Footer';
import Login from './Login/Login';
import Leavemanagement from './Leavemanagement/Leave';
import Createuser from './Createuser/User';
import Home from './Home/Home';
import Leavelist from './Leavelists/Leavelist';
import Userprofile from './Userprofile/Userprofile';
import Userlist from './Userlists/Userlist';
import tokensServ from './Token/Token-service';
import Attendance from './Attendance/Attendanceportal';

const PrivateRoute = (props) => {
  const Component = props.component;
  const data = tokensServ.tokenDecoded();
  const userRole = data.userRole;
  const token = tokensServ.token || null;
  if (token && userRole === 'admin') {
    return <Route path={props.path} render={(props) => <Component {...props} />}></Route>
  } else {
    return <Redirect to="/login"></Redirect>
  }
};

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Route exact path="/" component={Home}></Route>
          <Route path="/login" component={Login}></Route>
          <Route path="/user" component={Createuser}></Route>
          <Route path="/leave" component={Leavemanagement}></Route>
          <Route path="/about" component={About}></Route>
          <PrivateRoute path="/leaves" component={Leavelist}></PrivateRoute>
          <Route path="/userprofile" component={Userprofile}></Route>
          <PrivateRoute path="/userlist" component={Userlist}></PrivateRoute>
          <Route path="/attendance" component={Attendance}></Route>
          <Footer />
        </div>
      </Router>
    )
  }
}

export default App;
