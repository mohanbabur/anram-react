import React, { Component } from 'react';
import './Footer.css';
import { Link } from "react-router-dom";


class Footer extends Component {
    render() {
        return (
            <section className="container-fluid border-style mt-5 footer-back-color footer-bg-img ">
                <div className="row mt-5" >
                    <div className="col-md-4 mt-5">
                        <img className="logo" src="/Assets/Images/anram.png" alt="pic" />
                        <p className="text-center mt-4">Anram Solutions repeatedly delivers high quality, cost-effective services across
                            several industries.</p>
                        <ul className="social-icon-list footer-list-unstyled footer-list-inline text-center">
                            <li>
                                <Link className="footer-a-style" to="//www.facebook.com/vertisize">
                                    <i className="fa fa-facebook social-icons footer-socialicons"></i>
                                </Link>
                            </li>
                            <li>
                                <Link className="footer-a-style" to="https://twitter.com/vertisizeTech">
                                    <i className="fa fa-twitter social-icons footer-socialicons"></i>
                                </Link>
                            </li>
                            <li>
                                <Link className="footer-a-style" to="https://www.google.com/search?q=vertisize+solutions">
                                    <i className="fa fa-google-plus social-icons footer-socialicons"></i>
                                </Link>
                            </li>
                            <li>
                                <Link className="footer-a-style" to="https://www.pinterest.com/vertisizesoluti">
                                    <i className="fa fa-pinterest social-icons footer-socialicons"></i>
                                </Link>
                            </li>
                        </ul>
                    </div>


                    <div className="col-md-4 mt-5">
                        <h3 className="text-center">Useful <label className="text-danger">Links</label></h3>
                        <ul>
                            <li>
                                <Link to="#" className="text-white">Home</Link>
                            </li>
                            <li>
                                <Link to="#" className="text-white">About us</Link>
                            </li>
                            <li>
                                <Link to="#" className="text-white">Team</Link>
                            </li>
                            <li>
                                <Link to="#" className="text-white">Contact us</Link>
                            </li>
                            <li>
                                <Link to="#" className="text-white">Service</Link>
                            </li>
                        </ul>
                    </div>



                    <div className="col-md-4 mt-5">
                        <h3>
                            <label className="text-danger">Contact</label> us</h3>
                        <address>
                            <i className="fa fa-address-book social-icons"></i>
                            1st Floor, Plot No 33/B,
                            Road No 2, Banjara Hills,
                           <p>Behind Harley Davidson,
                            Hyderabad, India 500034.</p>
                        </address>

                        <i className="fa fa-phone social-icons m-3"></i>
                        <label>Phone: +91 040 4004 0440</label><br/>
                        <i className="fa fa-envelope social-icons m-3"></i>
                        <label>Email:anramsolution@gmail.com</label>
                    </div>
                </div>
                <div>
                    <hr />
                    <p className="text-center">© 2018 ALL RIGHTS RESERVED | ANRAM SOLUTIONS PVT.LTD,</p>
                </div>
            </section>
        )
    }
}

export default Footer;