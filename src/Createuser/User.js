import React, { Component } from 'react';
import './User.css';
import Userserv from '../Services/Auth/Auth-service';
import { withRouter } from 'react-router';
import Swal from 'sweetalert2';

class Createuser extends Component {
    data = {};
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.data[event.target.name] = event.target.value;
    }

    async handleSubmit(event) {
        event.preventDefault();
        event.target.reset();
        try {
            const result = await Userserv.createUser(this.data);
            console.log(result);
            Swal.fire({
                position: 'top-center',
                type: 'success',
                title: 'You have created account successfully in Anram',
                showConfirmButton: false,
                timer: 3000
            })
        }

        catch (error) {
            Swal.fire({
                position: 'top-center',
                type: 'error',
                title: 'Sorry you have already created account in Anram',
                showConfirmButton: false,
                timer: 3000
            })
        }

        // this.props.history.push('login');
    }
    render() {
        return (
            <div className="container-fluid ">
                <div className="row user-bg-img">
                    <div className="col-md-12 col-sm-12">
                        <h2 className=" text-center text-white my-4">NEW EMPLOYEE</h2>
                    </div>
                    <div className="row mx-auto d-block mb-5">
                        <div className="col-md-12 col-sm-12 user-bg pt-2">
                            <form className="form-group" name="createForm" onSubmit={this.handleSubmit}>
                                <div className="row">
                                    <div className="col-md-6">
                                        <label className="mt-2 user-label"> Name</label>
                                        <div>
                                            <input type="text" name="EmployeeName" id="First Name"
                                                className="form-control" required onChange={this.handleChange} />
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <label className="mt-2 user-label">Father Name</label>
                                        <div>
                                            <input type="text" name="FatherName" id="Father Name"
                                                className="form-control" required onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>
                                <div style={{ 'color': '#009688' }}>
                                    <label className="mt-2 user-label">Gender</label>
                                    <div>
                                        <input className="user-radio-style" type="radio" name="Gender" value="male" onChange={this.handleChange} />
                                        Male
                            <input className="user-radio-style" type="radio" name="Gender" value="female" onChange={this.handleChange} />
                                        Female
                            <input className="user-radio-style" type="radio" name="Gender" value="other" onChange={this.handleChange} />
                                        other

                        </div>
                                </div>
                                <div>
                                    <div>
                                        <label className="mt-2 user-label">Email</label>
                                        <div>
                                            <input type="email" name="Email" id="Email" className="form-control"
                                                required onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>


                                <div className="row">
                                    <div className="col-md-6">
                                        <label className="mt-2 user-label">AdhaarNo</label>
                                        <div>
                                            <input type="text" name="AdhaarNo" id="AdhaarNo" className="form-control"
                                                required onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-6  ">
                                        <label className="mt-2 user-label">PancardNo</label>
                                        <div>
                                            <input type="text" name="PancardNo" id="PancardNo"
                                                className="form-control" required onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <label className="mt-2 user-label">Mobile No</label>
                                        <div>
                                            <input type="text" name="EmployeeContactNo" id="EmployeeContactNo"
                                                className="form-control" required onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <label className="mt-2 user-label">Emergency No</label>
                                        <div>
                                            <input type="text" name="EmergencyContactNo"
                                                id="EmergencyContactNo" className="form-control" required onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <label className="mt-2 user-label">Temporary Address</label>
                                    <div>
                                        <textarea type="text" name="Address" id="Address" className="form-control"
                                            required onChange={this.handleChange}></textarea>
                                    </div>
                                </div>
                                <div>
                                    <label className="mt-2 user-label">PermanentAddress</label>
                                    <div>
                                        <textarea type="text" name="PermananentAddress" id="PermanentAddress"
                                            className="form-control" placeholder="PermanentAddress" required onChange={this.handleChange}></textarea>
                                    </div>
                                </div>


                                <div className="row justify-content-center text-center ">
                                    <div className="col-md-6 my-3">
                                        <button type="submit" className="btn">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Createuser);